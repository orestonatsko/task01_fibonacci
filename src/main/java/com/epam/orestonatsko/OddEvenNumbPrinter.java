package com.epam.orestonatsko;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>OddEvenNumbPrinter class.</h1>
 * Generates interval, finds sum of odd and even number,
 * divides at even and odd numbers
 * and prints results
 */
public final class OddEvenNumbPrinter implements NumbersPinter {
    /**
     * container for odd numbers.
     */
    private List<Integer> oddNumbers;
    /**
     * container for even numbers.
     */
    private List<Integer> evenNumbers;

    /**
     * private constructor OddEvenNumbPrinter that creates a new instance
     * in create method.
     *
     * @param start the number from which the interval begins
     * @param end   the number of which ends in interval
     */
    private OddEvenNumbPrinter(final int start, final int end) {
        generateNumberIntervals(start, end);
    }

    /**
     * this method generate even and odd number intervals.
     *
     * @param start the number from which the interval begins
     * @param end   the number of which ends in interval
     */
    private void generateNumberIntervals(final int start, final int end) {
        ArrayList<Integer> numberList = new ArrayList<Integer>();
        for (int i = start; i <= end; i++) {
            numberList.add(i);
        }
        evenNumbers(numberList);
        oddNumbers(numberList);

    }

    /**
     * this method prints all results that made by the printer.
     */
    public void print() {
        System.out.println("Odd numbers from start to end:");
        System.out.println(oddNumbers);
        System.out.println("Even numbers from end to start:");
        System.out.println(evenNumbers);
        System.out.println("Sum of all odd numbers: " + oddSum());
        System.out.println("Sum of all even numbers: " + evenSum());
    }

    /**
     * this method calculates the sum of even numbers.
     *
     * @return the sum of even numbers
     */
    private int evenSum() {
        int sum = 0;
        for (int evenNumb : evenNumbers) {
            sum += evenNumb;
        }
        return sum;
    }

    /**
     * this method calculates the sum of odd numbers.
     *
     * @return the sum of odd numbers
     */
    private int oddSum() {
        int sum = 0;
        for (int oddNumb : oddNumbers) {
            sum += oddNumb;
        }
        return sum;
    }

    /**
     * this method gets all even numbers from interval.
     *
     * @param interval set of all numbers
     */
    private void evenNumbers(final List<Integer> interval) {
        evenNumbers = new ArrayList<Integer>();
        for (int i = interval.size() - 1; i > 0; i--) {
            int number = interval.get(i);
            if (number % 2 == 0) {
                evenNumbers.add(number);
            }
        }
    }

    /**
     * this method gets all odd numbers from interval.
     *
     * @param interval set of all numbers
     */
    private void oddNumbers(final List<Integer> interval) {
        oddNumbers = new ArrayList<Integer>();
        for (Integer number : interval) {
            if (number % 2 != 0) {
                oddNumbers.add(number);
            }
        }
    }

    /**
     * this method creates the OddEvenNumbPrinter.
     *
     * @param input user's input
     * @return oddEvenNumbersPrinter for creating,
     * working, printing the interval
     */
    static OddEvenNumbPrinter create(final String input) {
        int start;
        int end;
        String trimInput = input.trim().replaceAll("\\s", "");
        if (trimInput.startsWith("[") && trimInput.endsWith("]")) {
            try {
                start = Integer.parseInt(
                        trimInput.substring(1, trimInput.indexOf(';')));
                end = Integer.parseInt(
                        trimInput.substring(trimInput.indexOf(';') + 1,
                                trimInput.indexOf(']')));
                if (start < end) {
                    return new OddEvenNumbPrinter(start, end);
                }
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
            }
        }
        return null;
    }
}
