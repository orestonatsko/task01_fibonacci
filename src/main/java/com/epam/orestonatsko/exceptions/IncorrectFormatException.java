package com.epam.orestonatsko.exceptions;

public class IncorrectFormatException extends NumberFormatException {
    public IncorrectFormatException(String msg){
        super(msg);
    }
}
