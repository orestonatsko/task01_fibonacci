package com.epam.orestonatsko;

import com.epam.orestonatsko.exceptions.IncorrectFormatException;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>FibonacciNumbPrinter class.</h1>
 * Generates fibonacci sequence, finds biggest odd and even number,
 * show even and odd numbers percentage
 * and prints results
 */

public final class FibonacciNumbPrinter implements NumbersPinter {
    /**
     * container for fibonacci numbers.
     */
    private List<Long> sequence;
    /**
     * this field for calculating percentage of even/odd numbers.
     */
    private static final int COEFFICIENT = 100;

    /**
     * private constructor FibonacciNumbPrinter
     * for creating fibonacciNumbersPrinter.
     *
     * @param set number of elements in fibonacci sequence
     */
    private FibonacciNumbPrinter(final int set) {
        createFibonacciSequence(set);
    }

    /**
     * this method creates fibonacci sequence.
     *
     * @param set number of elements in fibonacci sequence
     */
    private void createFibonacciSequence(final int set) {
        sequence = new ArrayList<Long>();
        for (int i = 0; i < set; i++) {
            sequence.add(getFibonacci(i));
        }
    }

    /**
     * this method generates fibonacci numbers.
     *
     * @param n number position of fibonacci sequence
     * @return fibonacci number
     */
    private long getFibonacci(final int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        long previous = 0;
        long next = 1;
        long sum = 0;
        for (int i = 2; i <= n; i++) {
            sum = previous + next;
            previous = next;
            next = sum;
        }
        return sum;
    }

    /**
     * this method prints all results that made by the printer.
     */
    public void print() {
        System.out.println("Fibonacci sequence:");
        System.out.println(sequence);
        System.out.println("Biggest odd number: " + getBiggestOddNumber());
        System.out.println("Biggest even number: " + getBiggestEvenNumber());
        System.out.println("Odd numbers: " + getOddPercentage() + "%;");
        System.out.println("Even numbers: " + getEvenPercentage() + "%;");
    }

    /**
     * this method calculates percentage of even numbers in sequence.
     *
     * @return percentage of even numbers
     */
    private int getEvenPercentage() {
        int evenCounter = 0;
        for (long fn : sequence) {
            if (fn % 2 == 0) {
                evenCounter++;
            }
        }
        return (evenCounter * COEFFICIENT) / sequence.size();
    }

    /**
     * this method calculates percentage of odd numbers in sequence.
     *
     * @return percentage of odd numbers
     */
    private int getOddPercentage() {
        int oddCounter = 0;
        for (long fn : sequence) {
            if (fn % 2 != 0) {
                oddCounter++;
            }
        }
        return (oddCounter * COEFFICIENT) / sequence.size();
    }

    /**
     * this method finds and returns biggest even number
     * from fibonacci sequence.
     *
     * @return biggest even number from fibonacci sequence
     */
    private long getBiggestEvenNumber() {
        long max = -1;
        for (Long aSequence : sequence) {
            long fNumber = aSequence;
            if (fNumber % 2 == 0 && max < aSequence) {
                max = aSequence;
            }
        }
        return max;
    }

    /**
     * this method finds and returns biggest ood number from fibonacci sequence.
     *
     * @return biggest odd number from fibonacci sequence
     */
    private long getBiggestOddNumber() {
        long max = -1;
        for (int i = 1; i < sequence.size(); i++) {
            long fNumb = sequence.get(i);
            if (fNumb % 2 != 0 && max < fNumb) {
                max = fNumb;
            }
        }
        return max;
    }

    /**
     * this method creates the fibonacciNumbersPrinter.
     *
     * @param size size of fibonacci sequence
     * @return fibonacciNumbersPrinter for creating,
     * working, printing the sequence
     */
    static FibonacciNumbPrinter create(final String size) {
        int s;
        try {
            s = Integer.parseInt(size);
            if (s < 0) {
                throw new IncorrectFormatException("Incorrect format for fibonacci sequence");
            }
        } catch (NumberFormatException e) {
            return null;
        }
        return new FibonacciNumbPrinter(s);
    }
}
