package com.epam.orestonatsko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * <h1>task01_fibonacci Program</h1>
 * <p>task01_fibonacci Program generates fibonacci numbers,
 * shows biggest even and odd number in sequence.
 * Also, shows percentage of even and odd numbers.</p>
 * <p>The program divides number interval into odd and even interval,
 * and calculates sum of odd and even numbers in interval.
 * The program prints all result in console</p>
 *

 */

public final class App {
    /**
     * private constructor App class.
     */
    private App() {
    }

    /**
     * this field for user's input.
     */
    private BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));
    /**
     * this filed for work with interval or fibonacci sequence.
     */
    private NumbersPinter numbersPinter;

    /**
     * The main method.
     *
     * @param args main args
     * @throws IOException handle incorrect input of user
     */
    public static void main(final String[] args) throws IOException {
        App app = new App();
        char mode;
        do {
            System.out.println("Select program mode: F/I");
            mode = app.getModeFromUserInput();
            if (mode == 'f' || mode == 'i') {
                app.numbersPinter = app.createNumbersPrinter(mode);
                if (app.numbersPinter != null) {
                    app.numbersPinter.print();
                } else {
                    app.error();
                }
            } else {
                app.error();
            }
            System.out.println("Quit the program? Press: y/n");
            mode = app.getModeFromUserInput();
        } while (mode != 'y');
    }

    /**
     * this method gets program mode from user input.
     *
     * @return the mode character
     */
    private char getModeFromUserInput() {
        try {
            String input = br.readLine();
            if (input != null && !input.equals("")) {
                return Character.toLowerCase(input.charAt(0));
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return '\u0000';
    }

    /**
     * this method creates NumberPrinter for creating and printing interval.
     *
     * @param mode the program mode that points what printer should create
     * @return created printer
     * @throws IOException handles incorrect input of user
     */
    private NumbersPinter createNumbersPrinter(final char mode)
            throws IOException {
        NumbersPinter printer;
        switch (mode) {
            case 'f':
                System.out.println("Enter the size of set fibonacci sequence:");
                printer = FibonacciNumbPrinter.create(br.readLine());
                break;
            case 'i':
                System.out.print("Enter start and end interval numbers ");
                System.out.println("in format: [1;100]:");
                printer = OddEvenNumbPrinter.create(br.readLine());
                break;
            default:
                return null;
        }
        return printer;
    }

    /**
     * this method shows that user's input is wrong.
     */
    private void error() {
        System.out.println("INCORRECT INPUT!");
    }
}
