package com.epam.orestonatsko;

/**
 * NumberPrinter interface that has common method print().
 */
public interface NumbersPinter {
    /**
     * this method prints result made by a printer.
     */
    void print();
}
